#![allow(non_snake_case)]
use leptos::*;
// use leptos_router::*;

#[component]
pub fn AdminIndex(cx: Scope) -> impl IntoView {
    view! { cx,
        <div id="admin-index">
            <h1>"Admin Index"</h1>
        </div>
    }
}